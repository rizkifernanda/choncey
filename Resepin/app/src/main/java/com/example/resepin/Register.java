package com.example.resepin;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Register extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
    }

    public void btnregister(View view) {
        final EditText firstname = (EditText) findViewById(R.id.txtfirstname);
        final EditText lastname = (EditText) findViewById(R.id.txtlastname);
        final EditText email = (EditText) findViewById(R.id.txtemail);
        final EditText password = (EditText) findViewById(R.id.txtpassword);

        String inputfirstname = firstname.getText().toString();
        String inputlastname = lastname.getText().toString();
        String inputemail = email.getText().toString();
        String inputpassword = password.getText().toString();

        if (TextUtils.isEmpty(inputfirstname)) {
            firstname.setError("First Name Harus Di Isi");
            firstname.requestFocus();
        } else if (TextUtils.isEmpty(inputlastname)) {
            lastname.setError("Last Name Harus Di Isi");
            lastname.requestFocus();
        } else if (TextUtils.isEmpty(inputemail)) {
            email.setError("E-mail Harus Di Isi");
            email.requestFocus();
        } else if (TextUtils.isEmpty(inputpassword)) {
            password.setError("Password Harus Di Isi");
            password.requestFocus();
        } else {
            final ProgressDialog loading = ProgressDialog.show(this,"", "Registering...", true);

            OkHttpClient client = new OkHttpClient();

            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("firstname", inputfirstname)
                    .addFormDataPart("lastname", inputlastname)
                    .addFormDataPart("email", inputemail)
                    .addFormDataPart("password", inputpassword)
                    .build();

            Request request = new Request.Builder()
                    .url("https://rizkifernanda.000webhostapp.com/register.php")
                    .post(requestBody)
                    .build();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(@NotNull Call call, @NotNull IOException e) {

                }

                @Override
                public void onResponse(@NotNull Call call, @NotNull final Response response) throws IOException {
                    final String result = response.body().string();

                    Register.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                JSONObject jsonResult = new JSONObject(result);
                                String status = jsonResult.getString("status");
                                if (status.equals("success")){
                                    Toast.makeText(Register.this, "Register success", Toast.LENGTH_LONG).show();
                                    finish();
                                } else {
                                    Toast.makeText(Register.this, "Register Failed", Toast.LENGTH_LONG).show();
                                }
                            } catch(Exception ex){
                                Toast.makeText(Register.this, ex.getMessage().toString(), Toast.LENGTH_LONG).show();
                            }
                            loading.cancel();
                        }
                    });
                }
            });
        }
    }
}
