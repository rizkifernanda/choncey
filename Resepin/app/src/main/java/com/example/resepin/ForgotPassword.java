package com.example.resepin;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

public class ForgotPassword extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotpassword);
    }

    public void btnresetpassword(View view) {
        final EditText emailaddress = (EditText) findViewById(R.id.txtenteremailaddress);

        String inputemail = emailaddress.getText().toString();

        if (TextUtils.isEmpty(inputemail)) {
            emailaddress.setError("E-mail Harus Di Isi");
            emailaddress.requestFocus();
        }
    }
}
