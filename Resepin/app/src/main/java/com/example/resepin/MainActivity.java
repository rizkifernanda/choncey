package com.example.resepin;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void btnLogin(View view) {
        final EditText _email = (EditText)findViewById(R.id.txtemail);
        final EditText _password = (EditText)findViewById(R.id.txtPassword);

        String email = _email.getText().toString();
        String password = _password.getText().toString();

        if (TextUtils.isEmpty(email)) {
            _email.setError("E-mail Harus Di Isi");
            _email.requestFocus();
        }else if (TextUtils.isEmpty(password)) {
            _password.setError("Password Harus Di Isi");
            _password.requestFocus();
        }else{
            final ProgressDialog loading = ProgressDialog.show(this,"", "Logging in...", true);

            OkHttpClient client = new OkHttpClient();

            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("email", email)
                    .addFormDataPart("password", password)
                    .build();

            Request request = new Request.Builder()
                    .url("https://rizkifernanda.000webhostapp.com/login.php")
                    .post(requestBody)
                    .build();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(@NotNull Call call, @NotNull IOException e) {

                }

                @Override
                public void onResponse(@NotNull Call call, @NotNull final Response response) throws IOException {
                    final String result = response.body().string();

                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                JSONObject jsonResult = new JSONObject(result);
                                String status = jsonResult.getString("status");
                                if (status.equals("success")){
                                    Toast.makeText(MainActivity.this, "Login success", Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(MainActivity.this, "Login Failed", Toast.LENGTH_LONG).show();
                                }
                            } catch(Exception ex){
                                Toast.makeText(MainActivity.this, ex.getMessage().toString(), Toast.LENGTH_LONG).show();
                            }
                            loading.cancel();
                        }
                    });
                }
            });
        }
    }

    public void txt_register(View view) {
        Intent intent = new Intent( this, Register.class);
        startActivity(intent);
    }

    public void txt_lupapassword(View view) {
        Intent intent = new Intent( this, ForgotPassword.class);
        startActivity(intent);
    }
}
