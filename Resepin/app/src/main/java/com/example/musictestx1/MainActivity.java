package com.example.musictestx1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void btnLogin(View view) {
        final EditText username = (EditText)findViewById(R.id.txtUsername);
        final EditText password = (EditText)findViewById(R.id.txtPassword);

        if (username.getText().toString().equals("admin") && password.getText().toString().equals("admin")){
            Toast.makeText(this, "Success!", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Failed", Toast.LENGTH_LONG).show();
        }
    }
}
