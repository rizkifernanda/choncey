package com.example.utstipeb;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


/**
 * A simple {@link Fragment} subclass.
 */
public class Maps extends Fragment {
    Button btnnormal, btnhybrid, btnsatelit, btnterrain;

    private GoogleMap mMap;
    private double longitude;
    private double latitude;

    public Maps() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_maps, container, false);

        //
        btnnormal = (Button) view.findViewById(R.id.btnnormal);
        btnnormal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            }
        });
        btnhybrid = (Button) view.findViewById(R.id.btnhybrid);
        btnhybrid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
            }
        });
        btnsatelit = (Button) view.findViewById(R.id.btnsatelit);
        btnsatelit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
            }
        });
        btnterrain = (Button) view.findViewById(R.id.btnterrain);
        btnterrain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
            }
        });
        //
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;



                CameraPosition googlePlex = CameraPosition.builder()
                        .target(new LatLng(-6.234112,106.7473))
                        .zoom(17)
                        .bearing(0)
                        .tilt(45)
                        .build();

                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(googlePlex), 6000, null);

                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(-6.234112,106.7473))
                        .title("Lab Universitas Budi Luhur"));
                /*LatLng kebonnanas = new LatLng(-6.2158432, 106.6304508);
                LatLng current = new LatLng(latitude, longitude);
                mMap.addMarker(new MarkerOptions().position(current).title("Kebon Nanas"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(current));


                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(kebonnanas, 17));*/
                mMap.getUiSettings().setZoomControlsEnabled(true);
                mMap.getUiSettings().setCompassEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
                mMap.setPadding(70, 100, 50, 150);
            }
        });
        return view;
    }

}
