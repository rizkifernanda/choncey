package com.example.utstipeb;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class PerpustakaanActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perpustakaan);
    }

    public void mahasiswa(View view) {
        Intent intent = new Intent(PerpustakaanActivity.this, Mahasiwa.class);
        startActivity(intent);

        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    public void dosen(View view) {
        Intent intent = new Intent(PerpustakaanActivity.this, Dosen.class);
        startActivity(intent);

        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    public void staff(View view) {
        Intent intent = new Intent(PerpustakaanActivity.this, Staff.class);
        startActivity(intent);

        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
}
