package com.example.utstipeb;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

public class KeamananKomputer extends AppCompatActivity {
    private TextView txtProgress1, txtProgress2, txtProgress3;
    private ProgressBar progressBar1, progressBar2, progressBar3;
    private int pStatus1 = 0, pStatus2 = 0, pStatus3 = 0;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_keamanan_komputer);

        txtProgress1 = (TextView) findViewById(R.id.txtProgress);
        progressBar1 = (ProgressBar) findViewById(R.id.progressBar);

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (pStatus1 <= 85) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressBar1.setProgress(pStatus1);
                            txtProgress1.setText(pStatus1 + " %");
                        }
                    });
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    pStatus1++;
                }
            }
        }).start();

        txtProgress2 = (TextView) findViewById(R.id.txtProgress2);
        progressBar2 = (ProgressBar) findViewById(R.id.progressBar2);

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (pStatus2 <= 75) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressBar2.setProgress(pStatus2);
                            txtProgress2.setText(pStatus2 + " %");
                        }
                    });
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    pStatus2++;
                }
            }
        }).start();

        txtProgress3 = (TextView) findViewById(R.id.txtProgress3);
        progressBar3 = (ProgressBar) findViewById(R.id.progressBar3);

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (pStatus3 <= 70) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressBar3.setProgress(pStatus3);
                            txtProgress3.setText(pStatus3 + " %");
                        }
                    });
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    pStatus3++;
                }
            }
        }).start();
    }

    /*public void absensi(View view) {
        txtProgress1 = (TextView) findViewById(R.id.txtProgress);
        progressBar1 = (ProgressBar) findViewById(R.id.progressBar);

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (pStatus1 <= 85) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressBar1.setProgress(pStatus1);
                            txtProgress1.setText(pStatus1 + " %");
                        }
                    });
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    pStatus1++;
                }
            }
        }).start();
    }

    public void uts(View view) {

        txtProgress2 = (TextView) findViewById(R.id.txtProgress2);
        progressBar2 = (ProgressBar) findViewById(R.id.progressBar2);

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (pStatus2 <= 75) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressBar2.setProgress(pStatus2);
                            txtProgress2.setText(pStatus2 + " %");
                        }
                    });
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    pStatus2++;
                }
            }
        }).start();
    }

    public void uas(View view) {
        txtProgress3 = (TextView) findViewById(R.id.txtProgress3);
        progressBar3 = (ProgressBar) findViewById(R.id.progressBar3);

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (pStatus3 <= 70) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressBar3.setProgress(pStatus3);
                            txtProgress3.setText(pStatus3 + " %");
                        }
                    });
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    pStatus3++;
                }
            }
        }).start();
    }*/
}
