package com.example.utstipeb;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class Matakuliah extends Fragment {
    Button btnijk2, btnetikaprofesi, btnrpl2, btnjk2, btnkeamanankomputer;

    public Matakuliah() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_matakuliah, container, false);

        btnijk2 = (Button) view.findViewById(R.id.btnijk2);
        btnijk2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Matakuliah.this.getActivity(), IJK2.class);
                startActivity(intent);
            }
        });

        btnetikaprofesi = (Button) view.findViewById(R.id.btnetikaprofesi);
        btnetikaprofesi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Matakuliah.this.getActivity(), EtikaProfesi.class);
                startActivity(intent);
            }
        });

        btnrpl2= (Button) view.findViewById(R.id.btnrpl2);
        btnrpl2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Matakuliah.this.getActivity(), RekayasaPerangkatLunak2.class);
                startActivity(intent);
            }
        });

        btnjk2= (Button) view.findViewById(R.id.btnjk2);
        btnjk2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Matakuliah.this.getActivity(), JaringanKomputer2.class);
                startActivity(intent);
            }
        });

        btnkeamanankomputer= (Button) view.findViewById(R.id.btnkeamanankomputer);
        btnkeamanankomputer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Matakuliah.this.getActivity(), KeamananKomputer.class);
                startActivity(intent);
            }
        });


        return view;
    }

}
