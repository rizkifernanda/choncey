package com.example.appgabungan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class AndroidActivity extends AppCompatActivity {
    Button applepie, bananabread, cupcake, donut, eclair, froyo, gingerbread, honeycomb, icecreamsandwich, jellybean, kitkat, lollipop, marshmallow, nougat, oreo, pie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_android);

        applepie = findViewById(R.id.applepie);
        applepie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AndroidActivity.this, ApplePieActivity.class));
            }
        });

        bananabread = findViewById(R.id.bananabread);
        bananabread.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AndroidActivity.this, BananaBreadActivity.class));
            }
        });

        cupcake = findViewById(R.id.cupcake);
        cupcake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AndroidActivity.this, CupcaakeActivity.class));
            }
        });

        donut = findViewById(R.id.donut);
        donut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AndroidActivity.this, DonutActivity.class));
            }
        });

        eclair = findViewById(R.id.eclair);
        eclair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AndroidActivity.this, EclairActivity.class));
            }
        });

        froyo = findViewById(R.id.froyo);
        froyo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AndroidActivity.this, FroyoActivity.class));
            }
        });

        gingerbread = findViewById(R.id.gingerbread);
        gingerbread.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AndroidActivity.this, GingerbreadActivity.class));
            }
        });

        honeycomb = findViewById(R.id.honeycomb);
        honeycomb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AndroidActivity.this, HoneycombActivity.class));
            }
        });

        icecreamsandwich = findViewById(R.id.icecreamsandwich);
        icecreamsandwich.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AndroidActivity.this, IcecreamsandwichActivity.class));
            }
        });

        jellybean = findViewById(R.id.jellybean);
        jellybean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AndroidActivity.this, JellybeanActivity.class));
            }
        });

        kitkat = findViewById(R.id.kitkat);
        kitkat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AndroidActivity.this, KitkatActivity.class));
            }
        });

        lollipop = findViewById(R.id.lollipop);
        lollipop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AndroidActivity.this, LollipopActivity.class));
            }
        });

        marshmallow = findViewById(R.id.marshmallow);
        marshmallow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AndroidActivity.this, MarshmallowActivity.class));
            }
        });

        nougat = findViewById(R.id.nougat);
        nougat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AndroidActivity.this, NougatActivity.class));
            }
        });

        oreo = findViewById(R.id.oreo);
        oreo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AndroidActivity.this, OreoActivity.class));
            }
        });

        pie = findViewById(R.id.pie);
        pie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AndroidActivity.this, PieActivity.class));
            }
        });
    }
}
