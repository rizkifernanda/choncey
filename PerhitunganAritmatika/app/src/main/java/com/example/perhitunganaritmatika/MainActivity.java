package com.example.perhitunganaritmatika;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onBackPressed(){
        AlertDialog.Builder tombolkeluar = new AlertDialog.Builder(MainActivity.this);
        tombolkeluar.setMessage("Apakah anda yakin ingin keluar dari Aplikasi ini? ");
        tombolkeluar.setTitle("Keluar Aplikasi");
        tombolkeluar.setIcon(R.drawable.alertbox);
        tombolkeluar.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                MainActivity.this.finish();
            }
        });

        tombolkeluar.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        tombolkeluar.setNeutralButton("Batal", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        tombolkeluar.show();
    }

    public void btnLingkaran_Luas(View view) {
        String judulDialog = "Lingkaran - Luas"; // Judul Dialog
        int v = R.layout.layout_lingkaran_luas; // Layout untuk Dialog

        //----------------//

        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(v);
        dialog.setTitle(judulDialog);
        // set the custom dialog components - text, image and button
        final EditText val_luas = (EditText) dialog.findViewById(R.id.txt_lingkaran_luas);
        final TextView ret_luas = (TextView) dialog.findViewById(R.id.lblLingkaranLuasHasil);
        Button dialogButton = (Button) dialog.findViewById(R.id.btnLingkaranLuasHitung);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String inputluas = val_luas.getText().toString();

                if (TextUtils.isEmpty(inputluas)) {
                    val_luas.setError("Luas Harus Di Isi");
                    val_luas.requestFocus();
                }else {
                    Double r = Double.parseDouble(val_luas.getText().toString());
                    Double ret = Math.PI * r * r;
                    String s = "Rumus =\nL = π . r2\nL = π . " + r + " . " + r + "\nHasil akhir = " + ret;
                    ret_luas.setText(s);
                }
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);

        final AlertDialog.Builder panjanglert = new AlertDialog.Builder (MainActivity.this);
        panjanglert.setMessage("Apakah anda yakin ingin menghitung luas lingkaran");
        panjanglert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.show();
            }
        });
        panjanglert.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        panjanglert.show();
    }

    public void btnLingkaran_Diameter(View view) {
        String judulDialog = "Lingkaran - Diameter"; // Judul Dialog
        int v = R.layout.layout_lingkaran_diameter; // Layout untuk Dialog

        //----------------//

        // custom dialog
        final Dialog dialog = new Dialog(this); // Variable untuk bikin dialog. gaperlu diubah
        dialog.setContentView(v); // Ambil template dari variable 'v'. gaperlu diubah
        dialog.setTitle(judulDialog); // Mengatur judul dialog. Ambil value string dari 'judulDialog' gaperlu diubah
        // set the custom dialog components - text, image and button
        final EditText val_diameter= (EditText) dialog.findViewById(R.id.txt_lingkaran_diameter); // Memilih EditText untuk mengambil value dari layout
        final TextView ret_diameter = (TextView) dialog.findViewById(R.id.lblLingkaranDiameterHasil); // Memilih TextView untuk menyimpan hasil hitunga=
        Button dialogButton = (Button) dialog.findViewById(R.id.btnLingkaranDiameterHitung); // Memilih Button untuk fungsi menghitung
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputdiameter= val_diameter.getText().toString();

                if (TextUtils.isEmpty(inputdiameter)) {
                    val_diameter.setError("Diameter Harus Di Isi");
                    val_diameter.requestFocus();
                }else {
                    Double r = Double.parseDouble(val_diameter.getText().toString()); // Variable dari EditText
                    Double ret = r * r; // Rumus dasar untuk langsung hitung
                    String s = "Rumus =\nd = 2r\nd = " + r + " . " + r + "\nHasil akhir = " + ret; // Variable sementara untuk hasil
                    ret_diameter.setText(s); // Menyimpan hasil dari variable 's' ke TextView
                }
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);

        final AlertDialog.Builder panjanglert = new AlertDialog.Builder (MainActivity.this);
        panjanglert.setMessage("Apakah anda yakin ingin menghitung diameter lingkaran");
        panjanglert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.show();
            }
        });
        panjanglert.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        panjanglert.show();
    }


    public void btnLingkaran_Keliling(View view) {
        String judulDialog = "Lingkaran - Keliling"; // Judul Dialog
        int v = R.layout.layout_lingkaran_keliling; // Layout untuk Dialog

        //----------------//

        // custom dialog
        final Dialog dialog = new Dialog(this); // Variable untuk bikin dialog. gaperlu diubah
        dialog.setContentView(v); // Ambil template dari variable 'v'. gaperlu diubah
        dialog.setTitle(judulDialog); // Mengatur judul dialog. Ambil value string dari 'judulDialog' gaperlu diubah
        // set the custom dialog components - text, image and button
        final EditText val_keliling = (EditText) dialog.findViewById(R.id.txt_lingkaran_keliling); // Memilih EditText untuk mengambil value dari layout
        final TextView ret_keliling = (TextView) dialog.findViewById(R.id.lblLingkaranKelilingHasil); // Memilih TextView untuk menyimpan hasil hitunga=
        Button dialogButton = (Button) dialog.findViewById(R.id.btnLingkaranKelilingHitung); // Memilih Button untuk fungsi menghitung
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputkeliling= val_keliling.getText().toString();

                if (TextUtils.isEmpty(inputkeliling)) {
                    val_keliling.setError("Diameter Harus Di Isi");
                    val_keliling.requestFocus();
                }else{
                    Double r = Double.parseDouble(val_keliling.getText().toString()); // Variable dari EditText
                    Double ret = 2 * Math.PI * r; // Rumus dasar untuk langsung hitung
                    String s = "Rumus = \n" +
                            "K = 2 . π . r\n" +
                            "K = 2. π . "+ r +"\nHasil Akhir =" + ret; // Variable sementara untuk hasil
                    ret_keliling.setText(s); // Menyimpan hasil dari variable 's' ke TextView
                }
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);

        final AlertDialog.Builder panjanglert = new AlertDialog.Builder (MainActivity.this);
        panjanglert.setMessage("Apakah anda yakin ingin menghitung keliling lingkaran");
        panjanglert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.show();
            }
        });
        panjanglert.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        panjanglert.show();
    }

    public void btnPersegi_Panjang_Luas(View view) {
        String judulDialog = "Persegi Panjang - Luas"; // Judul Dialog
        int v = R.layout.layout_persegi_panjang_luas; // Layout untuk Dialog

        //----------------//

        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(v);
        dialog.setTitle(judulDialog);
        // set the custom dialog components - text, image and button
        final EditText val_panjang = (EditText) dialog.findViewById(R.id.txt_persegi_panjang_panjang);
        final EditText val_lebar = (EditText) dialog.findViewById(R.id.txt_persegi_panjang_lebar);
        final TextView ret_luas = (TextView) dialog.findViewById(R.id.lblPersegiPanjangLuasHasil);
        Button dialogButton = (Button) dialog.findViewById(R.id.btnPersegiPanjangLuasHitung);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String inputluas = val_panjang.getText().toString();
                String inputlebar = val_lebar.getText().toString();

                if (TextUtils.isEmpty(inputluas)) {
                    val_panjang.setError("Luas Harus Di Isi");
                    val_panjang.requestFocus();
                } else if (TextUtils.isEmpty(inputlebar)) {
                    val_lebar.setError("Lebar Harus Di Isi");
                    val_lebar.requestFocus();
                }
                else {
                    Double p = Double.parseDouble(val_panjang.getText().toString());
                    Double l = Double.parseDouble(val_lebar.getText().toString());
                    Double ret = p * l;
                    String s = "Rumus =\nL = Panjang . Lebar\nL = " + p + " . " + l + "\nHasil akhir = " + ret;
                    ret_luas.setText(s);
                }
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);

        final AlertDialog.Builder panjanglert = new AlertDialog.Builder (MainActivity.this);
        panjanglert.setMessage("Apakah anda yakin ingin menghitung luas persegi panjang");
        panjanglert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.show();
            }
        });
        panjanglert.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        panjanglert.show();
    }

    public void btnPersegi_Panjang_Keliling(View view) {
        String judulDialog = "Persegi Panjang - Keliling"; // Judul Dialog
        int v = R.layout.layout_persegi_panjang_keliling; // Layout untuk Dialog

        //----------------//

        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(v);
        dialog.setTitle(judulDialog);
        // set the custom dialog components - text, image and button
        final EditText val_panjang = (EditText) dialog.findViewById(R.id.txt_persegi_panjang_keliling);
        final EditText val_lebar = (EditText) dialog.findViewById(R.id.txt_persegi_panjang_lebar);
        final TextView ret_luas = (TextView) dialog.findViewById(R.id.lblPersegiPanjangKelilingHasil);
        Button dialogButton = (Button) dialog.findViewById(R.id.btnPersegiPanjangKelilingHitung);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String inputpanjang = val_panjang.getText().toString();
                String inputlebar = val_lebar.getText().toString();

                if (TextUtils.isEmpty(inputpanjang)) {
                    val_panjang.setError("Luas Harus Di Isi");
                    val_panjang.requestFocus();
                } else if (TextUtils.isEmpty(inputlebar)) {
                    val_lebar.setError("Lebar Harus Di Isi");
                    val_lebar.requestFocus();
                }
                else {
                    Double p = Double.parseDouble(val_panjang.getText().toString());
                    Double l = Double.parseDouble(val_lebar.getText().toString());
                    Double ret = 2 * (p + l);
                    String s = "Rumus =\nL = 2 . (Panjang + Lebar)\nL = " + 2 +" . (" + p + " + " + l + ")\nHasil akhir = " + ret;
                    ret_luas.setText(s);
                }
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);


        final AlertDialog.Builder panjanglert = new AlertDialog.Builder (MainActivity.this);
        panjanglert.setMessage("Apakah anda yakin ingin menghitung keliling persegi panjang");
        panjanglert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.show();
            }
        });
        panjanglert.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        panjanglert.show();
    }

    public void btnBujur_Sangkar_Luas(View view) {
        String judulDialog = "Bujur Sangkar - Luas"; // Judul Dialog
        int v = R.layout.layout_bujur_sangkar_luas; // Layout untuk Dialog

        //----------------//

        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(v);
        dialog.setTitle(judulDialog);
        // set the custom dialog components - text, image and button
        final EditText val_luas = (EditText) dialog.findViewById(R.id.txt_bujur_sangkar_luas);
        final TextView ret_luas = (TextView) dialog.findViewById(R.id.lblBujurSangkarLuasHasil);
        Button dialogButton = (Button) dialog.findViewById(R.id.btnBujurSangkarLuasHitung);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String inputluas = val_luas.getText().toString();

                if (TextUtils.isEmpty(inputluas)) {
                    val_luas.setError("Luas Harus Di Isi");
                    val_luas.requestFocus();
                } else {
                    Double si = Double.parseDouble(val_luas.getText().toString());
                    Double ret = si * si;
                    String s = "Rumus =\nL = Sisi . Sisi\nL = " + si + " . " + si + "\nHasil akhir = " + ret;
                    ret_luas.setText(s);
                }
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);

        final AlertDialog.Builder panjanglert = new AlertDialog.Builder (MainActivity.this);
        panjanglert.setMessage("Apakah anda yakin ingin menghitung luas bujur sangkar");
        panjanglert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.show();
            }
        });
        panjanglert.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        panjanglert.show();
    }

    public void btnBujur_Sangkar_Keliling(View view) {
        String judulDialog = "Bujur Sangkar - Keliling"; // Judul Dialog
        int v = R.layout.layout_bujur_sangkar_keliling; // Layout untuk Dialog

        //----------------//

        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(v);
        dialog.setTitle(judulDialog);
        // set the custom dialog components - text, image and button
        final EditText val_luas = (EditText) dialog.findViewById(R.id.txt_bujur_sangkar_keliling);
        final TextView ret_luas = (TextView) dialog.findViewById(R.id.lblBujurSangkarKelilingHasil);
        Button dialogButton = (Button) dialog.findViewById(R.id.btnBujurSangkarKelilingHitung);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String inputluas = val_luas.getText().toString();

                if (TextUtils.isEmpty(inputluas)) {
                    val_luas.setError("Luas Harus Di Isi");
                    val_luas.requestFocus();
                } else {
                    Double si = Double.parseDouble(val_luas.getText().toString());
                    Double ret = 4 * si;
                    String s = "Rumus =\nL = 4 . Sisi\nL = " + 4 + " . " + si + "\nHasil akhir = " + ret;
                    ret_luas.setText(s);
                }
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);

        final AlertDialog.Builder panjanglert = new AlertDialog.Builder (MainActivity.this);
        panjanglert.setMessage("Apakah anda yakin ingin menghitung keliling bujur sangkar");
        panjanglert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.show();
            }
        });
        panjanglert.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        panjanglert.show();
    }

    public void btnSegitiga_Luas(View view) {
        String judulDialog = "Segitiga - Luas"; // Judul Dialog
        int v = R.layout.layout_segitiga_luas; // Layout untuk Dialog

        //----------------//

        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(v);
        dialog.setTitle(judulDialog);
        // set the custom dialog components - text, image and button
        final EditText val_alas = (EditText) dialog.findViewById(R.id.txt_segitiga_alas);
        final EditText val_tinggi = (EditText) dialog.findViewById(R.id.txt_segitiga_tinggi);
        final TextView ret_luas = (TextView) dialog.findViewById(R.id.lblSegitigaLuasHasil);
        Button dialogButton = (Button) dialog.findViewById(R.id.btnSegitigaLuasHitung);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String inputalas = val_alas.getText().toString();
                String inputtinggi = val_tinggi.getText().toString();

                if (TextUtils.isEmpty(inputalas)) {
                    val_alas.setError("Luas Harus Di Isi");
                    val_alas.requestFocus();
                } else if (TextUtils.isEmpty(inputtinggi)) {
                    val_tinggi.setError("Lebar Harus Di Isi");
                    val_tinggi.requestFocus();
                }
                else {
                    Double a = Double.parseDouble(val_alas.getText().toString());
                    Double t = Double.parseDouble(val_tinggi.getText().toString());
                    Double ret = a * t / 2;
                    String s = "Rumus =\nL = Alas . Tinggi / 2\nL = " + a +" . " + t + " / " + 2 + "\nHasil akhir = " + ret;
                    ret_luas.setText(s);
                }
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);

        final AlertDialog.Builder panjanglert = new AlertDialog.Builder (MainActivity.this);
        panjanglert.setMessage("Apakah anda yakin ingin menghitung luas segitiga");
        panjanglert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.show();
            }
        });
        panjanglert.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        panjanglert.show();
    }

    public void btnSegitiga_Keliling(View view) {
        String judulDialog = "Segitiga - Keliling"; // Judul Dialog
        int v = R.layout.layout_segitiga_keliling; // Layout untuk Dialog

        //----------------//

        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(v);
        dialog.setTitle(judulDialog);
        // set the custom dialog components - text, image and button
        final EditText val_keliling = (EditText) dialog.findViewById(R.id.txt_segitiga_keliling);
        final TextView ret_keliling = (TextView) dialog.findViewById(R.id.lblSegitigaKelilingHasil);
        Button dialogButton = (Button) dialog.findViewById(R.id.btnSegitigaKelilingHitung);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputkeliling= val_keliling.getText().toString();

                if (TextUtils.isEmpty(inputkeliling)) {
                    val_keliling.setError("Luas Harus Di Isi");
                    val_keliling.requestFocus();
                } else {
                    Double si = Double.parseDouble(val_keliling.getText().toString());
                    Double ret = si * si * si;
                    String s = "Rumus =\nL = Sisi . Sisi . Sisi\nL = " + si + " . " + si + " . " + si + "\nHasil akhir = " + ret;
                    ret_keliling.setText(s);
                }
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);

        final AlertDialog.Builder panjanglert = new AlertDialog.Builder (MainActivity.this);
        panjanglert.setMessage("Apakah anda yakin ingin menghitung keliling segitiga");
        panjanglert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.show();
            }
        });
        panjanglert.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        panjanglert.show();
    }

    public void btnJajar_Genjang_Luas(View view) {
        String judulDialog = "Jajar Genjang - Luas"; // Judul Dialog
        int v = R.layout.layout_jajar_genjang_luas; // Layout untuk Dialog

        //----------------//

        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(v);
        dialog.setTitle(judulDialog);
        // set the custom dialog components - text, image and button
        final EditText val_alas = (EditText) dialog.findViewById(R.id.txt_jajar_genjang_alas);
        final EditText val_tinggi = (EditText) dialog.findViewById(R.id.txt_jajar_genjang_tinggi);
        final TextView ret_luas = (TextView) dialog.findViewById(R.id.lblJajarGenjangLuasHasil);
        Button dialogButton = (Button) dialog.findViewById(R.id.btnJajarGenjangLuasHitung);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String inputalas = val_alas.getText().toString();
                String inputtinggi = val_tinggi.getText().toString();

                if (TextUtils.isEmpty(inputalas)) {
                    val_alas.setError("Luas Harus Di Isi");
                    val_alas.requestFocus();
                } else if (TextUtils.isEmpty(inputtinggi)) {
                    val_tinggi.setError("Lebar Harus Di Isi");
                    val_tinggi.requestFocus();
                }
                else {
                    Double a = Double.parseDouble(val_alas.getText().toString());
                    Double t = Double.parseDouble(val_tinggi.getText().toString());
                    Double ret = a * t;
                    String s = "Rumus =\nL = Alas . Tinggi\nL = " + a +" . " + t + "\nHasil akhir = " + ret;
                    ret_luas.setText(s);
                }
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);

        final AlertDialog.Builder panjanglert = new AlertDialog.Builder (MainActivity.this);
        panjanglert.setMessage("Apakah anda yakin ingin menghitung luas jajar genjang");
        panjanglert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.show();
            }
        });
        panjanglert.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        panjanglert.show();
    }

    public void btnJajar_Genjang_Keliling(View view) {
        String judulDialog = "Jajar Genjang - Keliling"; // Judul Dialog
        int v = R.layout.layout_jajar_genjang_keliling; // Layout untuk Dialog

        //----------------//

        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(v);
        dialog.setTitle(judulDialog);
        // set the custom dialog components - text, image and button
        final EditText val_alas = (EditText) dialog.findViewById(R.id.txt_jajar_genjang_alas);
        final EditText val_sisimiring = (EditText) dialog.findViewById(R.id.txt_jajar_genjang_sisi_miring);
        final TextView ret_luas = (TextView) dialog.findViewById(R.id.lblJajarGenjangKelilingHasil);
        Button dialogButton = (Button) dialog.findViewById(R.id.btnJajarGenjangKelilingHitung);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String inputalas = val_alas.getText().toString();
                String inputtinggi = val_sisimiring.getText().toString();

                if (TextUtils.isEmpty(inputalas)) {
                    val_alas.setError("Alas Harus Di Isi");
                    val_alas.requestFocus();
                } else if (TextUtils.isEmpty(inputtinggi)) {
                    val_sisimiring.setError("Sisi Miring Harus Di Isi");
                    val_sisimiring.requestFocus();
                }
                else {
                    Double a = Double.parseDouble(val_alas.getText().toString());
                    Double t = Double.parseDouble(val_sisimiring.getText().toString());
                    Double ret = 2 * (a + t);
                    String s = "Rumus =\nL = 2 . Alas . Sisi Miring\nL = " + 2 + " . ( " + a +" + " + t + " )\nHasil akhir = " + ret;
                    ret_luas.setText(s);
                }
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);

        final AlertDialog.Builder panjanglert = new AlertDialog.Builder (MainActivity.this);
        panjanglert.setMessage("Apakah anda yakin ingin menghitung keliling jajar genjang");
        panjanglert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.show();
            }
        });
        panjanglert.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        panjanglert.show();
    }

    public void btnTrapesium_Luas(View view) {
        String judulDialog = "Trapesium - Luas"; // Judul Dialog
        int v = R.layout.layout_trapesium_luas; // Layout untuk Dialog

        //----------------//

        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(v);
        dialog.setTitle(judulDialog);
        // set the custom dialog components - text, image and button
        final EditText val_tinggi = (EditText) dialog.findViewById(R.id.txt_trapesium_tinggi);
        final EditText val_sisi_sejajar = (EditText) dialog.findViewById(R.id.txt_trapesium_sisi_sejajar);
        final EditText val_sisisejajar = (EditText) dialog.findViewById(R.id.txt_trapesium_sisisejajar);
        final TextView ret_luas = (TextView) dialog.findViewById(R.id.lblTrapesiumTinggiHasil);
        Button dialogButton = (Button) dialog.findViewById(R.id.btnTrapesiumTinggiHitung);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String inputluas = val_tinggi.getText().toString();
                String inputsisi_sejajar = val_sisi_sejajar.getText().toString();
                String inputsisisejajar = val_sisisejajar.getText().toString();

                if (TextUtils.isEmpty(inputluas)) {
                    val_tinggi.setError("Luas Harus Di Isi");
                    val_tinggi.requestFocus();
                } else if (TextUtils.isEmpty(inputsisi_sejajar)) {
                    val_sisi_sejajar.setError("Sisi Sejajar Harus Di Isi");
                    val_sisi_sejajar.requestFocus();
                } else if (TextUtils.isEmpty(inputsisisejajar)) {
                    val_sisisejajar.setError("Sisi Sejajar Harus Di Isi");
                    val_sisisejajar.requestFocus();
                } else {
                    Double t = Double.parseDouble(val_tinggi.getText().toString());
                    Double si = Double.parseDouble(val_sisi_sejajar.getText().toString());
                    Double sij = Double.parseDouble(val_sisisejajar.getText().toString());
                    Double ret = (si + sij) * t / 2;
                    String s = "Rumus =\nL = ( a + b ) . t / 2 \nL = ( " + si + " + " + sij + " ) . " + t + " / " + 2 + "\nNilai Akhir = " + ret;
                    ret_luas.setText(s);
                }
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);

        final AlertDialog.Builder panjanglert = new AlertDialog.Builder (MainActivity.this);
        panjanglert.setMessage("Apakah anda yakin ingin menghitung luas trapesium");
        panjanglert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.show();
            }
        });
        panjanglert.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        panjanglert.show();
    }

    public void btnTrapesium_Keliling(View view) {
        String judulDialog = "Trapesium - Keliling"; // Judul Dialog
        int v = R.layout.layout_trapesium_keliling; // Layout untuk Dialog

        //----------------//

        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(v);
        dialog.setTitle(judulDialog);
        // set the custom dialog components - text, image and button
        final EditText val_sisi_sejajar = (EditText) dialog.findViewById(R.id.txt_trapesium_sisi_sejajar);
        final EditText val_sisisejajar = (EditText) dialog.findViewById(R.id.txt_trapesium_sisisejajar);
        final EditText val_sisi = (EditText) dialog.findViewById(R.id.txt_trapesium_sisi);
        final EditText val_sejajar = (EditText) dialog.findViewById(R.id.txt_trapesium_sejajar);
        final TextView ret_keliling = (TextView) dialog.findViewById(R.id.lblTrapesiumTinggiHasil);
        Button dialogButton = (Button) dialog.findViewById(R.id.btnTrapesiumTinggiHitung);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String inputsisi_sejajar = val_sisi_sejajar.getText().toString();
                String inputsisisejajar = val_sisisejajar.getText().toString();
                String inputsisi = val_sisi.getText().toString();
                String inputsejajar = val_sejajar.getText().toString();

                if (TextUtils.isEmpty(inputsisi_sejajar)) {
                    val_sisi_sejajar.setError("Sisi Sejajar Harus Di Isi");
                    val_sisi_sejajar.requestFocus();
                } else if (TextUtils.isEmpty(inputsisisejajar)) {
                    val_sisisejajar.setError("Sisi Sejajar Harus Di Isi");
                    val_sisisejajar.requestFocus();
                } else if (TextUtils.isEmpty(inputsisi)) {
                    val_sisisejajar.setError("Sisi Sejajar Harus Di Isi");
                    val_sisisejajar.requestFocus();
                } else if (TextUtils.isEmpty(inputsejajar)) {
                    val_sejajar.setError("Sisi Sejajar Harus Di Isi");
                    val_sejajar.requestFocus();
                } else {
                    Double s_j = Double.parseDouble(val_sisi_sejajar.getText().toString());
                    Double sj = Double.parseDouble(val_sisisejajar.getText().toString());
                    Double si = Double.parseDouble(val_sisi.getText().toString());
                    Double sij = Double.parseDouble(val_sejajar.getText().toString());
                    Double ret = s_j + sj + si + sij;
                    String s = "Rumus =\nL = AB + BC + CD + DA \nL = " + s_j + " + " + sj + " + " + si + " + " + sij + "\nNilai Akhir = " + ret;
                    ret_keliling.setText(s);
                }
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);

        final AlertDialog.Builder panjanglert = new AlertDialog.Builder (MainActivity.this);
        panjanglert.setMessage("Apakah anda yakin ingin menghitung keliling trapesium");
        panjanglert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.show();
            }
        });
        panjanglert.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        panjanglert.show();
    }

    public void btnTrapesium_Volume (View view) {
        String judulDialog = "Trapesium - Volume";
        int v = R.layout.layout_trapesium_volume;

        //----------------//

        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(v);
        dialog.setTitle(judulDialog);
        // set the custom dialog components - text, image and button
        final EditText val_tinggivolume = (EditText) dialog.findViewById(R.id.txt_trapesium_tinggi_volume);
        final EditText val_sisi_sejajarvolume = (EditText) dialog.findViewById(R.id.txt_trapesium_sisi_sejajar_volume);
        final EditText val_sisisejajarvolume = (EditText) dialog.findViewById(R.id.txt_trapesium_sisi_sejajarvolume);
        final EditText val_tinggiprisma = (EditText) dialog.findViewById(R.id.txt_trapesium_tinggi_prisma);
        final TextView ret_luasalas = (TextView) dialog.findViewById(R.id.lblTrapesiumLuasAlasHasil);
        final TextView ret_volume = (TextView) dialog.findViewById(R.id.lblTrapesiumVolumeHasil);
        Button dialogButton = (Button) dialog.findViewById(R.id.btnTrapesiumVolumeHitung);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String inputtinggivolume = val_tinggivolume.getText().toString();
                String inputsisi_sejajarvolume = val_sisi_sejajarvolume.getText().toString();
                String inputsisisejajarvolume = val_sisisejajarvolume.getText().toString();
                String inputtinggiprisma = val_tinggiprisma.getText().toString();

                if (TextUtils.isEmpty(inputtinggivolume)) {
                    val_tinggivolume.setError("Tinggi Volume Harus Di Isi");
                    val_tinggivolume.requestFocus();
                } else if (TextUtils.isEmpty(inputsisi_sejajarvolume)) {
                    val_sisi_sejajarvolume.setError("Sisi Sejajar Harus Di Isi");
                    val_sisi_sejajarvolume.requestFocus();
                } else if (TextUtils.isEmpty(inputsisisejajarvolume)) {
                    val_sisisejajarvolume.setError("Sisi Sejajar Harus Di Isi");
                    val_sisisejajarvolume.requestFocus();
                } else if (TextUtils.isEmpty(inputtinggiprisma)) {
                    val_tinggiprisma.setError("Tinggi Prisma Harus Di Isi");
                    val_tinggiprisma.requestFocus();
                } else {
                    Double t1 = Double.parseDouble(val_tinggivolume.getText().toString());
                    Double si1 = Double.parseDouble(val_sisi_sejajarvolume.getText().toString());
                    Double sij1 = Double.parseDouble(val_sisisejajarvolume.getText().toString());
                    Double tp = Double.parseDouble(val_tinggiprisma.getText().toString());
                    Double ret = 0.5 *((si1 + sij1) * t1);
                    String s = "Rumus =\nLuas Alas = 1/2 * ( a + b ) . t \nLuas Alas = "  + "1/2" + " . ( " + si1 + " + " + sij1 + " ) . " + t1 + "\nNilai Luas Alas Trapesium = " + ret;
                    ret_luasalas.setText(s);

                    Double rethasil = ret * tp;
                    String s1 = "Volume = Luas Alas * Tinggi Prisma \nVolume = " + ret + "." + tp + "\nHasil Volume = "+rethasil;
                    ret_volume.setText(s1);
                }
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);

        final AlertDialog.Builder panjanglert = new AlertDialog.Builder (MainActivity.this);
        panjanglert.setMessage("Apakah anda yakin ingin menghitung luas trapesium");
        panjanglert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.show();
            }
        });
        panjanglert.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        panjanglert.show();
    }
}
