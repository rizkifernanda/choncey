package com.example.stasiuntv;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

public class ListChannel extends AppCompatActivity {
    protected  static Uri video;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_channel);
    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Press BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 1000);
    }

    public void RCTI_Button(View view) {
        video = android.net.Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.rcti);
        Intent intent = new Intent(this, videoActivity.class);
        startActivity(intent);
    }

    public void SCTV_Button(View view) {
        video = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.sctv);
        Intent intent = new Intent(this, videoActivity.class);
        startActivity(intent);
    }

    public void ANTV_Button(View view) {
        video = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.antv);
        Intent intent = new Intent(this, videoActivity.class);
        startActivity(intent);
    }

    public void RTV_Button(View view) {
        video = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.rtv);
        Intent intent = new Intent(this, videoActivity.class);
        startActivity(intent);
    }

    public void METROTV_Button(View view) {
        video = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.metrotv);
        Intent intent = new Intent(this, videoActivity.class);
        startActivity(intent);
    }

    public void SPACETOON_Button(View view) {
        video = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.spacetoon);
        Intent intent = new Intent(this, videoActivity.class);
        startActivity(intent);
    }

    public void TVONE_Button(View view) {
        video = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.tvone);
        Intent intent = new Intent(this, videoActivity.class);
        startActivity(intent);
    }

    public void TVRI_Button(View view) {
        video = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.tvri);
        Intent intent = new Intent(this, videoActivity.class);
        startActivity(intent);
    }

}
