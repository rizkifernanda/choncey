package com.example.stasiuntv;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Spinner spinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spinner = (Spinner) findViewById(R.id.spinnerChannel);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new String[]{"Channel Indonesia", "Channel International"});
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    public void btnPilih(View view) {
        int selected = spinner.getSelectedItemPosition();
        switch (selected) {
            case 0:
                Intent intent = new Intent(this, ListChannel.class);
                startActivity(intent);
                finish();
                break;

            default:
                Toast.makeText(getApplicationContext(), "Unavailable", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
