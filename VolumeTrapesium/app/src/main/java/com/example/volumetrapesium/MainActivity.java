package com.example.volumetrapesium;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final LinearLayout btnVolumeTrapesium = findViewById(R.id.btnTrapesium_Volume);
        btnVolumeTrapesium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String judulDialog = "Trapesium - Volume";
                int v = R.layout.volumetrapesium; // Layout untuk Dialog

                //----------------//

                // custom dialog
                final Dialog dialog = new Dialog(MainActivity.this);
                dialog.setContentView(v);
                dialog.setTitle(judulDialog);
                // set the custom dialog components - text, image and button
                final EditText val_sisi_sejajarvolume = dialog.findViewById(R.id.txt_trapesium_sisi_sejajar_volume);
                final EditText val_sisisejajarvolume = dialog.findViewById(R.id.txt_trapesium_sisi_sejajarvolume);
                final EditText val_tinggivolume = dialog.findViewById(R.id.txt_trapesium_tinggi_volume);
                final EditText val_tinggiprisma = dialog.findViewById(R.id.txt_trapesium_tinggi_prisma);
                final TextView ret_luasalas = dialog.findViewById(R.id.lblTrapesiumLuasAlasHasil);
                final TextView ret_volume = dialog.findViewById(R.id.lblTrapesiumVolumeHasil);
                Button dialogButton = dialog.findViewById(R.id.btnTrapesiumVolumeHitung);
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String inputsisi_sejajarvolume = val_sisi_sejajarvolume.getText().toString();
                        String inputsisisejajarvolume = val_sisisejajarvolume.getText().toString();
                        String inputtinggivolume = val_tinggivolume.getText().toString();
                        String inputtinggiprisma = val_tinggiprisma.getText().toString();

                        if (TextUtils.isEmpty(inputsisi_sejajarvolume)) {
                            val_sisi_sejajarvolume.setError("Sisi Sejajar Harus Di Isi");
                            val_sisi_sejajarvolume.requestFocus();
                        } else if (TextUtils.isEmpty(inputsisisejajarvolume)) {
                            val_sisisejajarvolume.setError("Sisi Sejajar Harus Di Isi");
                            val_sisisejajarvolume.requestFocus();
                        } else if (TextUtils.isEmpty(inputtinggivolume)) {
                            val_tinggivolume.setError("Tinggi Volume Harus Di Isi");
                            val_tinggivolume.requestFocus();
                        } else if (TextUtils.isEmpty(inputtinggiprisma)) {
                            val_tinggiprisma.setError("Tinggi Prisma Harus Di Isi");
                            val_tinggiprisma.requestFocus();
                        } else {
                            Double t1 = Double.parseDouble(val_tinggivolume.getText().toString());
                            Double si1 = Double.parseDouble(val_sisi_sejajarvolume.getText().toString());
                            Double sij1 = Double.parseDouble(val_sisisejajarvolume.getText().toString());
                            Double tp = Double.parseDouble(val_tinggiprisma.getText().toString());
                            Double ret = 0.5 *((si1 + sij1) * t1);
                            String s = "Rumus =\nLuas Alas = 1/2 * ( a + b ) . t \nLuas Alas = "  + "1/2" + " . ( " + si1 + " + " + sij1 + " ) . " + t1 + "\nNilai Luas Alas Trapesium = " + ret;
                            ret_luasalas.setText(s);

                            Double rethasil = ret * tp;
                            String s1 = "Volume = Luas Alas * Tinggi Prisma \nVolume = " + ret + "." + tp + "\nHasil Volume = "+rethasil;
                            ret_volume.setText(s1);
                        }
                    }
                });
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                dialog.getWindow().setAttributes(lp);

                final AlertDialog.Builder panjanglert = new AlertDialog.Builder (MainActivity.this);
                panjanglert.setMessage("Apakah anda yakin ingin menghitung volume trapesium");
                panjanglert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialog.show();
                    }
                });
                panjanglert.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
                panjanglert.show();
            }
        });

    }

}
