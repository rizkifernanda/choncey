package com.example.playapp;

import android.app.ActivityOptions;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.playapp.ui.main.SectionsPagerAdapter;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
        FloatingActionButton fab = findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    public void mitosbtn(View view){
        Uri uri = Uri.parse("https://www.instagram.com/p/B2ZKooDhQnJ/");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
    public void ngopibtn(View view){
        Uri uri = Uri.parse("https://www.instagram.com/p/B2d0NHFhhTD/");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
    public void spongebobbtn(View view) {
        Uri uri = Uri.parse("https://www.instagram.com/p/B2ilIAehmwE/");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
    public void duniabtn(View view) {
        Uri uri = Uri.parse("https://www.instagram.com/p/B2qUm0Zh1IQ/");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
    public void demobtn(View view) {
        Uri uri = Uri.parse("https://www.instagram.com/p/B20KaeShGOD/");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
    public void infobtn(View view) {
        Uri uri = Uri.parse("https://www.instagram.com/p/B2yfZ73hKUo/");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
    public void misteribtn(View view) {
        Uri uri = Uri.parse("https://www.instagram.com/p/B0NcchWBkd0/");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
    public void uasbtn(View view) {
        Uri uri = Uri.parse("https://www.instagram.com/p/BzcGeF6h72A/");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }


    public void listWhatsAppMessenger(View view){
        Intent intent = new Intent( this, ActivityWhatsAppMessenger.class);
        startActivity(intent);

        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
    public void listFacebookLite(View view){
        Intent intent = new Intent( this, ActivityFacebookLite.class);
        startActivity(intent);

        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
    public void listShopee(View view){
        Intent intent = new Intent( this, ActivityShopee.class);
        startActivity(intent);

        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
    public void listYouTubeGo(View view){
        Intent intent = new Intent( this, ActivityYouTubeGo.class);
        startActivity(intent);

        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
    public void listSnapchat(View view){
        Intent intent = new Intent( this, ActivitySnapchat.class);
        startActivity(intent);

        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
    public void listIflix(View view){
        Intent intent = new Intent( this, ActivityIflix.class);
        startActivity(intent);

        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
    public void listSpotify(View view){
        Intent intent = new Intent( this, ActivitySpotify.class);
        startActivity(intent);

        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    public void listTixId(View view){
        Intent intent = new Intent( this, ActivityTixId.class);
        startActivity(intent);

        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }


    public void listCrisisAction(View view){
        Intent intent = new Intent( this, ActivityCrisisAction.class);
        startActivity(intent);

        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
    public void listClashofClans(View view){
        Intent intent = new Intent( this, ActivityClashofClans.class);
        startActivity(intent);

        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
    public void listGrandTheftAutoSanAndreas(View view){
        Intent intent = new Intent( this, ActivityGrandTheftAutoSanAndreas.class);
        startActivity(intent);

        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
    public void listCallofDuty(View view){
        Intent intent = new Intent( this, ActivityCallofDuty.class);
        startActivity(intent);

        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
    public void listMobileLegendsAdventure(View view){
        Intent intent = new Intent( this, ActivityMobileLegendsAdventure.class);
        startActivity(intent);

        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
    public void listPointBlankStrike(View view){
        Intent intent = new Intent( this, ActivityPointBlankStrike.class);
        startActivity(intent);

        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
    public void listMinecraft(View view){
        Intent intent = new Intent( this, ActivityMinecraft.class);
        startActivity(intent);

        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
    public void listSlitherio(View view){
        Intent intent = new Intent( this, ActivitySlitherio.class);
        startActivity(intent);

        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
}