package com.example.playapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class ActivityTixId extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.actionbar);

        setContentView(R.layout.activity_tix_id);
    }

    public void visitbtn(View view) {
        Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=id.tix.android");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

    public void s1(View view) {
        ImageView iv1 = findViewById(R.id.s1);
        ImageView iv2 = findViewById(R.id.s2);
        ImageView iv3 = findViewById(R.id.s3);
        ImageView iv4 = findViewById(R.id.s4);
        ImageView iv5 = findViewById(R.id.s5);

        iv1.setImageResource(R.drawable.s_yellow);
        iv2.setImageResource(R.drawable.s_gray);
        iv3.setImageResource(R.drawable.s_gray);
        iv4.setImageResource(R.drawable.s_gray);
        iv5.setImageResource(R.drawable.s_gray);
    }
    public void s2(View view) {
        ImageView iv1 = findViewById(R.id.s1);
        ImageView iv2 = findViewById(R.id.s2);
        ImageView iv3 = findViewById(R.id.s3);
        ImageView iv4 = findViewById(R.id.s4);
        ImageView iv5 = findViewById(R.id.s5);

        iv1.setImageResource(R.drawable.s_yellow);
        iv2.setImageResource(R.drawable.s_yellow);
        iv3.setImageResource(R.drawable.s_gray);
        iv4.setImageResource(R.drawable.s_gray);
        iv5.setImageResource(R.drawable.s_gray);
    }
    public void s3(View view) {
        ImageView iv1 = findViewById(R.id.s1);
        ImageView iv2 = findViewById(R.id.s2);
        ImageView iv3 = findViewById(R.id.s3);
        ImageView iv4 = findViewById(R.id.s4);
        ImageView iv5 = findViewById(R.id.s5);

        iv1.setImageResource(R.drawable.s_yellow);
        iv2.setImageResource(R.drawable.s_yellow);
        iv3.setImageResource(R.drawable.s_yellow);
        iv4.setImageResource(R.drawable.s_gray);
        iv5.setImageResource(R.drawable.s_gray);
    }
    public void s4(View view) {
        ImageView iv1 = findViewById(R.id.s1);
        ImageView iv2 = findViewById(R.id.s2);
        ImageView iv3 = findViewById(R.id.s3);
        ImageView iv4 = findViewById(R.id.s4);
        ImageView iv5 = findViewById(R.id.s5);

        iv1.setImageResource(R.drawable.s_yellow);
        iv2.setImageResource(R.drawable.s_yellow);
        iv3.setImageResource(R.drawable.s_yellow);
        iv4.setImageResource(R.drawable.s_yellow);
        iv5.setImageResource(R.drawable.s_gray);
    }
    public void s5(View view) {
        ImageView iv1 = findViewById(R.id.s1);
        ImageView iv2 = findViewById(R.id.s2);
        ImageView iv3 = findViewById(R.id.s3);
        ImageView iv4 = findViewById(R.id.s4);
        ImageView iv5 = findViewById(R.id.s5);

        iv1.setImageResource(R.drawable.s_yellow);
        iv2.setImageResource(R.drawable.s_yellow);
        iv3.setImageResource(R.drawable.s_yellow);
        iv4.setImageResource(R.drawable.s_yellow);
        iv5.setImageResource(R.drawable.s_yellow);
    }
}
