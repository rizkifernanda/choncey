package com.example.galgal;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity {
    String[] namapemain = {"David De Gea", "Diogo Dalot", "Harry Maguire", "Victor Lindelöf", "Luke Shaw", "Paul Pogba", "Scott McTominay", "Andreas Pereira", "Daniel James", "Anthony Martial", "Marcus Rashford"};
    int[] gambarpemain = {R.drawable.degea, R.drawable.dalot, R.drawable.maguire, R.drawable.lindelof, R.drawable.shaw, R.drawable.pogba, R.drawable.mctominay, R.drawable.pereira, R.drawable.daniel, R.drawable.martial, R.drawable.rashford};
    String[] posisipemain = {"Goal Keeper 1", "Right Black 20", "Center Back 5", "Center Back 2", "Left Back 23", "Central Midfielder 6", "Central Defensive Midfielder 39", "Right Winger 15", "Left Winger 21", "Left Winger 9", "Striker 10"};
    ViewPager pagerku;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        ViewPager myRel = (ViewPager) findViewById(R.id.mainpager);
        myRel.getBackground().setAlpha(76);
        /*getSupportActionBar().setElevation(0);
        getSupportActionBar().setTitle(Html.fromHtml("<font color='#D81920'>Manchester United Player</font>"));*/

        pagerku = findViewById(R.id.mainpager);
        pagerku.setAdapter(new fotoadapter(namapemain, gambarpemain, posisipemain, MainActivity.this));
        pagerku.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }
}
