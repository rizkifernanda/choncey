package com.example.galgal;

import android.app.Activity;
import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

class fotoadapter extends PagerAdapter {
    String[] adapternamapemain;
    int[] adaptergambarpemain;
    String[] adapterposisipemain;
    Activity adapteractivity;

    public fotoadapter(String[] namapemain, int[] gambarpemain, String[] posisipemain, MainActivity mainActivity) {
        adapternamapemain = namapemain;
        adaptergambarpemain = gambarpemain;
        adapterposisipemain = posisipemain;
        adapteractivity = mainActivity;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position){
        LayoutInflater inflaterfoto = (LayoutInflater) adapteractivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewfoto = inflaterfoto.inflate(R.layout.layout_utama, container, false);
        TextView textView = viewfoto.findViewById(R.id.textpager);
        TextView textView1 = viewfoto.findViewById(R.id.textpager1);
        ImageView imageView = viewfoto.findViewById(R.id.imagepager);
        textView.setText(adapternamapemain[position]);
        textView1.setText(adapterposisipemain[position]);
        imageView.setImageResource(adaptergambarpemain[position]);
        container.addView(viewfoto);
        return viewfoto;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object){
        container.removeView((LinearLayout)object);
    }

    @Override
    public int getCount() {
        return adaptergambarpemain.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((LinearLayout)object);
    }
}
