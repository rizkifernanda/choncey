package com.example.crudsqlite2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityLogin extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void btnLogin(View view) {
        final EditText username = (EditText)findViewById(R.id.txtusername);
        final EditText password = (EditText)findViewById(R.id.txtpassword);

        if (username.getText().toString().equals("c") && password.getText().toString().equals("c")){
            Toast.makeText(this, "Success!", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(ActivityLogin.this, MainActivity.class);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(this, "Failed", Toast.LENGTH_LONG).show();
        }
    }

}
