package com.example.firstapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.AlteredCharSequence;
import android.view.LayoutInflater;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /*public void mhs(View view) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setTitle("Test");
        alertDialog.setMessage("Rizki Fernanda Mantap");
        alertDialog.show();

    } */

    public void mhs(View view) {
        Intent intent = new Intent(this, MhsActivity.class);
        startActivity(intent);
    }

    public void jurusan(View view) {
        Intent intent = new Intent(this, JurusanActivity.class);
        startActivity(intent);
    }

    public void matkul(View view) {
        Intent intent = new Intent( this, MatkulActivity.class);
        startActivity(intent);
    }

    public void nilai(View view) {
        Intent intent = new Intent(this, NilaiActivity.class);
        startActivity(intent);
    }

    public void kkp(View view) {
        Intent intent = new Intent( this, KKPActivity.class);
        startActivity(intent);
    }

    public void skripsi(View view) {
        Intent intent = new Intent( this, SkripsiActivity.class);
        startActivity(intent);
    }
}
